if [ $BRANCH_NAME = 'dev' ]
then
    gcloud compute instances stop petclinic-dev --zone "us-east1-b"
elif [ $BRANCH_NAME = 'master' ]
then
cat >hosts.txt <<EOL
[production]
35.243.220.29 ansible_user=ansible
EOL

cat >free_port.sh <<EOL
if [ ! -z "\$(lsof -t -i:8080)" ] ;
then
kill "\$(lsof -t -i:8080)"
fi
EOL

ansible -i hosts.txt production -m copy -a "src=free_port.sh dest=free_port.sh"
ansible -i hosts.txt production -m shell -a "sh free_port.sh" -b
else
    heroku ps:scale web=0 --app petclinic-jenkins-pr
fi
