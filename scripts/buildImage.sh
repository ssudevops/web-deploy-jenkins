cp /var/jenkins_home/workspace/Build/petclinic/target/spring-petclinic-2.0.0.BUILD-SNAPSHOT.jar petclinic.jar

CURRENT_DATE=$(date +%Y%m%d%H%M%S)
echo CURRENT_DATE=$CURRENT_DATE > date.properties

touch Dockerfile
cat <<EOF> Dockerfile
FROM ascdc/jdk8

ADD petclinic.jar home/petclinic/petclinic.jar
ADD scripts home/petclinic/scripts
EXPOSE 8080

CMD ["java","-jar","home/petclinic/petclinic.jar"]

EOF


cat <<EOF> cloudbuild.yaml
steps:
- name: 'gcr.io/cloud-builders/docker'
  args: [ 'build', '-t', 'gcr.io/devops-cicd-219411/ssu_devops-image:$CURRENT_DATE', '.' ]
images:
- 'gcr.io/devops-cicd-219411/ssu_devops-image:$CURRENT_DATE'

EOF

gcloud builds submit --config cloudbuild.yaml .
