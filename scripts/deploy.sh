cat >free_port.sh <<EOL
if [ ! -z "\$(lsof -t -i:8080)" ] ;
then
kill "\$(lsof -t -i:8080)"
fi
EOL


if echo $ARTIFACT_DIR | egrep '^dev'
then
    gcloud compute instances start petclinic-dev --zone "us-east1-b"
    gcloud compute scp --project "devops-cicd-219411" --zone "us-east1-b" *.jar jenkins@petclinic-dev:~/petclinic.jar
    gcloud compute scp --project "devops-cicd-219411" --zone "us-east1-b" free_port.sh jenkins@petclinic-dev:~/
    gcloud compute --project "devops-cicd-219411" ssh --zone "us-east1-b" jenkins@petclinic-dev --command "sh free_port.sh"
    gcloud compute --project "devops-cicd-219411" ssh --zone "us-east1-b" jenkins@petclinic-dev --command "nohup java -jar petclinic.jar > log.txt 2>&1 &"
elif echo $ARTIFACT_DIR | egrep '^master'
then
cat >hosts.txt <<EOL
    [production]
    35.243.220.29 ansible_user=ansible
EOL

cat >playbook.yml <<EOL
---
- name: Copy jar and run it
  hosts: production
    
  tasks:
  - name: Copy jar
    copy: src={{ item }} dest=petclinic.jar
    with_fileglob: "*.jar"
  
  - name: Copy free_port.sh
    copy: src=free_port.sh dest=./
  
  - name: Free port 8080
    shell: sh free_port.sh
    become: yes
    
  - name: Run jar
    shell: nohup java -jar petclinic.jar > log.txt 2>&1 &
EOL
    ansible-playbook -i hosts.txt playbook.yml
else
    heroku deploy:jar *.jar --app petclinic-jenkins-pr  
fi
