gcloud beta compute instances update-container pet \
--zone us-east1-b \
--container-image gcr.io/devops-cicd-219411/ssu_devops-image:$CURRENT_DATE

gcloud compute instances start pet \
--zone us-east1-b

gcloud beta compute instances describe pet \
--zone us-east1-b