CURRENT_DATE=$(date +%Y%m%d%H%M%S)
export CURRENT_DATE
echo CURRENT_DATE=$CURRENT_DATE > env.properties
cd petclinic
mvn clean package
echo 'web: java $JAVA_OPTS -jar -Dserver.port=$PORT petclinic-'$CURRENT_DATE'.jar' > target/Procfile
gsutil copy -r ../scripts gs://petclinic-storage/${BRANCH_NAME}/$CURRENT_DATE/
gsutil copy target/Procfile gs://petclinic-storage/${BRANCH_NAME}/$CURRENT_DATE/Procfile
gsutil copy target/spring-petclinic-2.0.0.BUILD-SNAPSHOT.jar gs://petclinic-storage/${BRANCH_NAME}/$CURRENT_DATE/petclinic-$CURRENT_DATE.jar
