
# Документация по созданному CI/CD:

Полная схема CI/CD в примитивном виде выглядит так:
![Схема](https://i.imgur.com/Qmb6suJ.png)

1.  Создание pull-request'а, подхватывание его с помощью jenkins hook'а.
2. После того как был создан commit и отправлен в виде pull-request, Jenkins подхватывает это изменение
и начинает запускать поочередно джобы Build, Deploy и DeployDO. В зависимости от того в какую ветку было внесено
изменение будут запускаться различные джобы (Build или BuildImage).

3.  Развертывание созданного артефакта на одной из платформ.
В нашем случае может быть создан один из следующих артефактов собранный .jar файл или docker-контейнер.
Соответственно .jar разворачивается на платформе Heroku, а Docker-контейнер разворачивается на google cloud engine
виртуальных машинах. Соотвественно если все завершилось успешно, то в pull-request отображается успешный build.

4. Развертывание созданного .jar файла на Digital Ocean.

После того как все успешно развернулось необходимо запустить terminate, то есть удалить все созданные ресурсы и завершить работу запущенных виртуальных машин.

## Build-процесс
### Более детальный обзор самого build процесса.
За процесс сборки артефакта у нас отвечает два скрипта *build.sh* и *buildImage.sh*.
Так же в jenkins'е мы имеем несколько build-джоб, которые и используют эти скрипты.
Разберем по порядку что делает jenkins *Build* джоба.

1.  Скачивает репозиторий с petclinic и скриптами.
2.  Сохраняется в переменную среды текущая дата, для того чтобы задать некую версию будущей сброки.
2.  Запускает скрипт *build.sh*, в котором начинается сброка проекта.
3.  После того как проект был собран мы записываем команду запуска этого проекта в *Procfile*, этот файл необходим нам для запуска проекта на платформе Heroku.
4.  Далее мы используем gcp-утилиту gutil для копирования всех созданных данных на наш google storage (Procfile, .jar-файл) название проекта и папки соотвествует дате создания артефакта.

На этом процесс build заканчивается. Как правило данная джоба используется для сборки пакетов из ветки master, которые используются в production-среде.

## Deploy-процесс
За процесс развертывания web-приложения отвечают скрипты *deploy.sh* и *deployPetCloud.sh*. Им соответствуют одноименные джобы на Jenkins.
Принцип работы Deploy джобы.

1. Перед тем как разворачивать приложение необходимо проверить занят ли порт 8080. Для этих целей создается отдельный скрипт *free_port.sh*, который завершает все процессы, которые используют данный порт.
2. Далее процесс разворачивания разветвляется, в зависимости от сброки dev-ветки или master.
3.  Если это dev, то в google cloud создается виртуальная машина, на которой и будет находится приложение.
* Создается инстанс, на него скачиваются все артефакты и скрипты.
* На этом инстансе запускается скрипт *free_port.sh*.
* Запускается сам проект, при этом весь процесс запуска записывается в файл *logs.txt*
4. Если это master, то данный артефакт будет разворачиваться на Heroku. На Heroku имеется несколько приложений (apps) - это petclinic-jenkins и petclinic-jenkins-pr.



### Принцип работы buildImage
*BuildImage.sh* скрипт соотвественно используется джобой BuildImage. Данная джоба использует build джобу для того, чтобы взять .jar файл.
Данная джоба оборачивает наш .jar файл в контейнер. Это необходимо для удобного разворачивания приложения на вирутальных машинах google cloud.

Принцип работы.

1. Если buildImage была запущена сама, то она забирает последний успешный артефакт build джобы с gcloud storage в виде .jar файла.
2. Как и в build джобы создается timestamp, который служит версией сборки.
3. Создается dockerfile, который представляет собой набор инструкций, необходимый для построения docker контейнера.
4. В этот dockerfile добавляется image oraclejdk, который необходим для нашего проекта.
5. Так же в этот dockerfile добавляется .jar файл, скрипты и открывается порт 8080, чтобы приложение было доступно из вне.

### DeployImage

Принцип работы.

1. Необходимо запустить build джобу для получения .jar файла. Если она успешно отработала, то тогда запускается buildImage.
2. В gcloud создается инстанс, на который скачивается наш артефакт в виде docker image.
3. Скачанный image запускается, после чего весь процесс запуска отображается.

## Terminate-процесс

За процесс остановки web-приложения отвечает скрипт terminate.sh. После того как все успешно развернулось необходимо запустить terminate, чтобы удалить все созданные ресурсы и завершить работу запущенных виртуальных машин.
Принцип работы terminate.

1. Процесс завершения работы разветвляется, в зависимости от сброки dev-ветки или master.
2. Если это ветка dev, то созданные ранее инстансы останавливаются, а виртуальная машина будет навсегда удалена.
3. Если это ветка master, то завершается процесс, на заданном порту 8080, развернутый на Heroku. На Heroku имеется несколько приложений (apps) - это petclinic-jenkins и petclinic-jenkins-pr.

-- // --


**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).

